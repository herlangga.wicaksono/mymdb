package com.herlangga.wicaksono.mymdb.data.repositories

import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem

interface MovieCacheDataSource {
    suspend fun getMoviesFromCache(): List<MovieItem>
    suspend fun saveMoviesToCache(movies: List<MovieItem>)
}