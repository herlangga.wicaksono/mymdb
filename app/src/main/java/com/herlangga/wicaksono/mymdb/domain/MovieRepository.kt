package com.herlangga.wicaksono.mymdb.domain

import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem

interface MovieRepository {
    suspend fun getMovies(page: Int): List<MovieItem>?
    suspend fun updateMovies(page: Int):List<MovieItem>?
}