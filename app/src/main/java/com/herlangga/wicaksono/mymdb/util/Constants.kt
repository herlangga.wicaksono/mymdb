package com.herlangga.wicaksono.mymdb.util

class Constants {
    companion object {
        const val API_KEY = "752da370a9e0d7aef6429ecd10d5569b"
        const val BASE_URL = "https://api.themoviedb.org/3/"
        const val SEARCH_NEWS_TIME_DELAY = 500L
        const val QUERY_PAGE_SIZE = 20
    }
}