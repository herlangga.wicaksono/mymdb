package com.herlangga.wicaksono.mymdb.di.core

import com.herlangga.wicaksono.mymdb.data.api.MyService
import com.herlangga.wicaksono.mymdb.data.repositories.MovieCacheDataSourceImpl
import com.herlangga.wicaksono.mymdb.data.repositories.MovieRemoteDataSource
import com.herlangga.wicaksono.mymdb.data.repositories.MovieRemoteDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RemoteDataModule(private val apiKey: String) {

    @Singleton
    @Provides
    fun provideMovieRemoteDataSource(myService: MyService): MovieRemoteDataSource{
        return MovieRemoteDataSourceImpl(
            myService, apiKey
        )
    }
}