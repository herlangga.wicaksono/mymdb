package com.herlangga.wicaksono.mymdb.di.movie

import com.herlangga.wicaksono.mymdb.domain.MovieUseCase
import com.herlangga.wicaksono.mymdb.persentation.movie.MovieViewModelFactory
import dagger.Module
import dagger.Provides

@Module
class MovieModule {

    @MovieScope
    @Provides
    fun provideMovieVieModelFactory(movieUseCase: MovieUseCase): MovieViewModelFactory {
        return MovieViewModelFactory(movieUseCase)
    }

}