package com.herlangga.wicaksono.mymdb.data.repositories

import com.herlangga.wicaksono.mymdb.data.api.MyService
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieListItem
import retrofit2.Response

class MovieRemoteDataSourceImpl(private val myService: MyService, private val apiKey: String): MovieRemoteDataSource {
    override suspend fun getMovies(page: Int): Response<MovieListItem> {
        return myService.getPopularMovies(apiKey, page)
    }
}