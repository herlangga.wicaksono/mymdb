package com.herlangga.wicaksono.mymdb.di.core

import com.herlangga.wicaksono.mymdb.data.api.MyService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule(private val baseUrl: String) {

    @Singleton
    @Provides
    fun provideRetrofit(): Retrofit {
        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(baseUrl)
            .build()
    }

    @Singleton
    @Provides
    fun provideService(retrofit: Retrofit): MyService {
        return retrofit.create(MyService::class.java)
    }
}