package com.herlangga.wicaksono.mymdb.domain

import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem

class MovieUseCase(private val repositories: MovieRepository) {

    suspend fun getMovies(page: Int):List<MovieItem>? = repositories.getMovies(page)
    suspend fun updateMovies(page: Int):List<MovieItem>? = repositories.updateMovies(page)

}