package com.herlangga.wicaksono.mymdb.persentation.movie

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.liveData
import androidx.lifecycle.viewModelScope
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem
import com.herlangga.wicaksono.mymdb.domain.MovieUseCase
import com.herlangga.wicaksono.mymdb.util.Resource
import kotlinx.coroutines.launch

class MovieViewModel(private val useCase: MovieUseCase) : ViewModel() {

    private var page = 1

    val movieList:
            MutableLiveData<Resource<List<MovieItem>>> = MutableLiveData()
    private var movieListTemp: MutableList<MovieItem> = mutableListOf()

    fun getMovies() = viewModelScope.launch {
        movieList.postValue(Resource.Loading())
        val response = useCase.getMovies(page)
        movieList.postValue(handleMovieResponse(response))

    }

    private fun handleMovieResponse(response: List<MovieItem>?): Resource<List<MovieItem>> {
        page++
        response?.let { movieListTemp.addAll(it) }
        return Resource.Success(movieListTemp)
    }

    fun updateMovies() = liveData {
        val movieList = useCase.updateMovies(page)
        emit(movieList)
    }

}