package com.herlangga.wicaksono.mymdb.di.core

import com.herlangga.wicaksono.mymdb.data.repositories.MovieCacheDataSource
import com.herlangga.wicaksono.mymdb.data.repositories.MovieRemoteDataSource
import com.herlangga.wicaksono.mymdb.data.repositories.MovieRepositoryImpl
import com.herlangga.wicaksono.mymdb.domain.MovieRepository
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Provides
    @Singleton
    fun provideMovieRepository(
        movieRemoteDataSource: MovieRemoteDataSource,
        movieCacheDataSource: MovieCacheDataSource
    ): MovieRepository {
        return MovieRepositoryImpl(
            movieRemoteDataSource,
            movieCacheDataSource
        )
    }

}