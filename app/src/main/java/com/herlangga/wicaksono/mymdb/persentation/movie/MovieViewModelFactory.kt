package com.herlangga.wicaksono.mymdb.persentation.movie

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.herlangga.wicaksono.mymdb.domain.MovieUseCase

class MovieViewModelFactory(
    private val useCase: MovieUseCase
) : ViewModelProvider.Factory{
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return MovieViewModel(useCase) as T
    }

}