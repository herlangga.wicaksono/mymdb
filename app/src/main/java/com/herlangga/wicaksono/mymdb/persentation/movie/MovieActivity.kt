package com.herlangga.wicaksono.mymdb.persentation.movie

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AbsListView
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.herlangga.wicaksono.mymdb.R
import com.herlangga.wicaksono.mymdb.databinding.ActivityMainBinding
import com.herlangga.wicaksono.mymdb.di.Injector
import com.herlangga.wicaksono.mymdb.util.Constants.Companion.QUERY_PAGE_SIZE
import com.herlangga.wicaksono.mymdb.util.Resource
import javax.inject.Inject

class MovieActivity : AppCompatActivity() {

    @Inject
    lateinit var factory: MovieViewModelFactory
    private lateinit var vm: MovieViewModel
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter: MovieAdapter//MovieAdapter

    var isLoading = false
    var isLastPage = false
    var isScrolling = false

    val scrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)

            val layoutManager = recyclerView.layoutManager as LinearLayoutManager
            val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()
            val visibleItemCount = layoutManager.childCount
            val totalItemCount = layoutManager.itemCount

            val isNotLoadingAndNotLastPage = !isLoading && !isLastPage
            val isAtLastItem = firstVisibleItemPosition + visibleItemCount >= totalItemCount
            val isNotAtBeginning = firstVisibleItemPosition >= 0
            val isTotalMoreThanVisible = totalItemCount >= QUERY_PAGE_SIZE
            val shouldPaginate = isNotLoadingAndNotLastPage && isAtLastItem && isNotAtBeginning &&
                    isTotalMoreThanVisible && isScrolling
            if (shouldPaginate) {
                vm.getMovies()
                isScrolling = false
            } else {
                binding.rvMovie.setPadding(0, 0, 0, 0)
            }
        }

        override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
            super.onScrollStateChanged(recyclerView, newState)
            if (newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
                isScrolling = true
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        (application as Injector).createMovieSubComponent().inject(this)
        vm = ViewModelProvider(this, factory).get(MovieViewModel::class.java)

        initRecyclerView()
        initData()
        initObserver()
    }

    private fun initData() {
        vm.getMovies()
    }

    private fun initRecyclerView() {
//        adapter = MovieAdapter()
//        binding.rvMovie.apply {
//            adapter = adapter
//            layoutManager = LinearLayoutManager(context)
//            addOnScrollListener(scrollListener)
//        }
        binding.rvMovie.layoutManager = LinearLayoutManager(this)
        adapter = MovieAdapter()
        binding.rvMovie.adapter = adapter
        binding.rvMovie.addOnScrollListener(scrollListener)
    }

    private fun showLoading(){
        binding.progressBar.visibility = View.VISIBLE
        isLoading = true
    }

    private fun hideLoading(){
        binding.progressBar.visibility = View.GONE
        isLoading = false
    }

    private fun initObserver() {
        vm.movieList.observe(this, Observer {
            when (it) {
                is Resource.Loading -> {
                    showLoading()
                }
                is Resource.Success -> {
                    hideLoading()
                    Log.i("elang","elang ${it.data.toString()}")

                    if (!it.data.isNullOrEmpty()){
                        adapter.differ.submitList(it.data)
                        adapter.notifyDataSetChanged()
                    }
                }
                is Resource.Error -> {
                    hideLoading()
                }
            }
        })
    }
}