package com.herlangga.wicaksono.mymdb.di.core

import android.graphics.Movie
import com.herlangga.wicaksono.mymdb.domain.MovieRepository
import com.herlangga.wicaksono.mymdb.domain.MovieUseCase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class UseCaseModule {

    @Provides
    fun provideMovieUseCase(repository: MovieRepository): MovieUseCase{
        return MovieUseCase(repository)
    }
}