package com.herlangga.wicaksono.mymdb.persentation.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.herlangga.wicaksono.mymdb.R
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem
import com.herlangga.wicaksono.mymdb.databinding.ItemMovieListBinding
import com.herlangga.wicaksono.mymdb.util.loadImage

class PopularMovieAdapter(): RecyclerView.Adapter<MyViewHolder>() {

    private val movieList = ArrayList<MovieItem>()

    fun setList(data: List<MovieItem>){
        movieList.clear()
        movieList.addAll(data)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding: ItemMovieListBinding = DataBindingUtil.inflate(
                layoutInflater,
                R.layout.item_movie_list,
                parent,
                false)

        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.bind(movieList[position])
    }

    override fun getItemCount(): Int = movieList.size
}

class MyViewHolder(val binding: ItemMovieListBinding): RecyclerView.ViewHolder(binding.root){

    fun bind(data: MovieItem){
        binding.apply {
            data.apply {
                ivMovie.loadImage("https://image.tmdb.org/t/p/w500$posterPath", false)
                tvTitle.text = title
            }
        }
    }
}