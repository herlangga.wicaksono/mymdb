package com.herlangga.wicaksono.mymdb.data.repositories

import android.util.Log
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieListItem
import com.herlangga.wicaksono.mymdb.domain.MovieRepository
import retrofit2.Response

class MovieRepositoryImpl(
    private val movieRemoteDataSource: MovieRemoteDataSource,
    private val movieCacheDataSource: MovieCacheDataSource
) : MovieRepository {

    private var currentPage = 1

    override suspend fun getMovies(page: Int): List<MovieItem>? {
        return getMovieCache(page)
    }

    override suspend fun updateMovies(page: Int): List<MovieItem>? {
        val newListMovie: List<MovieItem> = getMovieApi(page)
        movieCacheDataSource.saveMoviesToCache(newListMovie)
        return newListMovie
    }

    suspend fun getMovieApi(page: Int): List<MovieItem>{
        lateinit var movieList: List<MovieItem>

        try {
            val response: Response<MovieListItem> = movieRemoteDataSource.getMovies(page)
            val body: MovieListItem? = response.body()
            if(body!=null){
                movieList = body.movies!!
            }
        }catch (exception:Exception){
        }

        return movieList
    }

    suspend fun getMovieCache(page: Int): List<MovieItem>{
        lateinit var movieList: List<MovieItem>

        try {
            movieList = movieCacheDataSource.getMoviesFromCache()
        }catch (exception:Exception){

        }

        if (movieList.isNotEmpty() && page == currentPage){
            return movieList
        } else{
            currentPage = page
            movieList = getMovieApi(page)
            movieCacheDataSource.saveMoviesToCache(movieList)
        }

        return movieList
    }
}