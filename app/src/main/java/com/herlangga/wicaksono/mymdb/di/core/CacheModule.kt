package com.herlangga.wicaksono.mymdb.di.core

import com.herlangga.wicaksono.mymdb.data.repositories.MovieCacheDataSource
import com.herlangga.wicaksono.mymdb.data.repositories.MovieCacheDataSourceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class CacheModule {

    @Singleton
    @Provides
    fun provideMovieCacheDataSource(): MovieCacheDataSource{
        return MovieCacheDataSourceImpl()
    }
}