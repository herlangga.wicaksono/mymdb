package com.herlangga.wicaksono.mymdb.persentation.movie

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.herlangga.wicaksono.mymdb.R
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem
import com.herlangga.wicaksono.mymdb.databinding.ItemMovieListBinding
import com.herlangga.wicaksono.mymdb.util.loadImage

class MovieAdapter() : RecyclerView.Adapter<MovieAdapter.MovieViewHolder>() {

    private lateinit var binding: ItemMovieListBinding

    private val differCallback = object : DiffUtil.ItemCallback<MovieItem>() {
        override fun areItemsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: MovieItem, newItem: MovieItem): Boolean {
            return oldItem == newItem
        }
    }

    val differ = AsyncListDiffer(this, differCallback)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MovieViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        binding = DataBindingUtil.inflate(layoutInflater, R.layout.item_movie_list, parent, false)
        return MovieViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MovieViewHolder, position: Int) {
        holder.bind(differ.currentList[position])
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class MovieViewHolder(val binding: ItemMovieListBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(data: MovieItem) {
            binding.apply {
                data.apply {
                    ivMovie.loadImage("https://image.tmdb.org/t/p/w500$posterPath", false)
                    tvTitle.text = title
                }
            }
        }
    }


}