package com.herlangga.wicaksono.mymdb.data.api

import com.herlangga.wicaksono.mymdb.data.models.movie.MovieListItem
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface MyService {
    @GET("movie/popular")
    suspend fun getPopularMovies(@Query("api_key") apiKey: String, @Query("page") page: Int): Response<MovieListItem>
}