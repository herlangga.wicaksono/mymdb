package com.herlangga.wicaksono.mymdb.data.models.movie


import com.google.gson.annotations.SerializedName

data class MovieListItem(
    @SerializedName("page")
    val page: Int?,
    @SerializedName("results")
    val movies: List<MovieItem>?,
    @SerializedName("total_pages")
    val totalPages: Int?,
    @SerializedName("total_results")
    val totalResults: Int?
)