package com.herlangga.wicaksono.mymdb

import android.app.Application
import com.herlangga.wicaksono.mymdb.di.Injector
import com.herlangga.wicaksono.mymdb.di.core.*
import com.herlangga.wicaksono.mymdb.di.movie.MovieSubComponent
import com.herlangga.wicaksono.mymdb.util.Constants.Companion.API_KEY
import com.herlangga.wicaksono.mymdb.util.Constants.Companion.BASE_URL

class MyMDB : Application(), Injector {

    private lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .networkModule(NetworkModule(BASE_URL))
            .remoteDataModule(RemoteDataModule(API_KEY))
            .build()
    }

    override fun createMovieSubComponent(): MovieSubComponent {
        return appComponent.movieSubComponent().create()
    }
}