package com.herlangga.wicaksono.mymdb.di

import com.herlangga.wicaksono.mymdb.di.movie.MovieSubComponent

interface Injector {
    fun createMovieSubComponent(): MovieSubComponent
}