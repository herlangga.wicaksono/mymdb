package com.herlangga.wicaksono.mymdb.data.repositories

import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem

class MovieCacheDataSourceImpl : MovieCacheDataSource {

    private var movieList = ArrayList<MovieItem>()

    override suspend fun getMoviesFromCache(): List<MovieItem> {
        return movieList
    }

    override suspend fun saveMoviesToCache(movies: List<MovieItem>) {
        movieList.clear()
        movieList = ArrayList(movies)
    }
}