package com.herlangga.wicaksono.mymdb.data.repositories

import android.graphics.pdf.PdfDocument
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieListItem
import retrofit2.Response

interface MovieRemoteDataSource {

    suspend fun getMovies(page: Int): Response<MovieListItem>

}