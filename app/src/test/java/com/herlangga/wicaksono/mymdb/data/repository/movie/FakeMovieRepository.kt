package com.herlangga.wicaksono.mymdb.data.repository.movie

import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem
import com.herlangga.wicaksono.mymdb.domain.MovieRepository

class FakeMovieRepository : MovieRepository {

    private val movieList = mutableListOf<MovieItem>()

    init {
        movieList.add(
            MovieItem(
                id = 1,
                overview = "overiew 2",
                posterPath = "posterPath 2",
                releaseDate = "releaseDate 2",
                title = "title1 2"
            )
        )
        movieList.add(
            MovieItem(
                id = 2,
                overview = "overiew 2",
                posterPath = "posterPath 2",
                releaseDate = "releaseDate 2",
                title = "title 1"
            )
        )
    }

    override suspend fun getMovies(page: Int): List<MovieItem>? {
        return movieList
    }

    override suspend fun updateMovies(page: Int): List<MovieItem>? {
        movieList.clear()
        movieList.add(
            MovieItem(
                id = 1,
                overview = "overiew 3",
                posterPath = "posterPath 3",
                releaseDate = "releaseDate 3",
                title = "title1 3"
            )
        )
        movieList.add(
            MovieItem(
                id = 2,
                overview = "overiew 4",
                posterPath = "posterPath 4",
                releaseDate = "releaseDate 5",
                title = "title 4"
            )
        )

        return movieList
    }
}