package com.herlangga.wicaksono.mymdb.persentation.movie

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.google.common.truth.Truth.assertThat
import com.herlangga.wicaksono.mymdb.MainCoroutineRule
import com.herlangga.wicaksono.mymdb.data.models.movie.MovieItem
import com.herlangga.wicaksono.mymdb.data.repository.movie.FakeMovieRepository
import com.herlangga.wicaksono.mymdb.domain.MovieUseCase
import com.herlangga.wicaksono.mymdb.getOrAwaitValueTest
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@ExperimentalCoroutinesApi
class MovieViewModelTest{


    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var mainCoroutineRule = MainCoroutineRule()

    private lateinit var vm: MovieViewModel

    @Before
    fun setUp(){
        val fakeMovieRepository = FakeMovieRepository()
        val useCase = MovieUseCase(fakeMovieRepository)

        vm = MovieViewModel(useCase)
    }

    @Test
    fun getMovies_returnList(){
        val movieList: MutableList<MovieItem> = mutableListOf<MovieItem>()
        movieList.add(
            MovieItem(
                id = 1,
                overview = "overiew 2",
                posterPath = "posterPath 2",
                releaseDate = "releaseDate 2",
                title = "title1 2"
            )
        )
        movieList.add(
            MovieItem(
                id = 2,
                overview = "overiew 2",
                posterPath = "posterPath 2",
                releaseDate = "releaseDate 2",
                title = "title 1"
            )
        )

        vm.getMovies()

        val currentList = vm.movieList.getOrAwaitValueTest()

        assertThat(currentList.data).isEqualTo(movieList)

    }
}